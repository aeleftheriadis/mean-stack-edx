var mongoose = require('mongoose');

var schema = new mongoose.Schema({
  name: { type: String, required: true },
  courses: [{ type: String, ref: 'Course' }]
});

/* Returns the student's first name, which we will define
 * to be everything up to the first space in the student's name.
 * For instance, "William Bruce Bailey" -> "William" */
schema.virtual('firstName').get(function() {
  //sreturn 'Not Implemented!';

  return this.name.split(' ')[0]//this.name.substr(0,this.name.indexOf(' '));
});

/* Returns the student's last name, which we will define
 * to be everything after the last space in the student's name.
 * For instance, "William Bruce Bailey" -> "Bailey" */
schema.virtual('lastName').get(function() {
  //return 'Not Implemented!';
  var split = this.name.split(' ');

  return split[split.length-1];
  //return this.name.substr(-1,this.name.indexOf(' '));
});

module.exports = schema;
